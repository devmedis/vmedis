# Release Notes Vmedis
* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [1.8.0.7] - 2018-08-08
## Menu Pengaturan Transaksi
- **Added :**
	1. Ada peringatan untuk konfirmasi kepada user ketika mengaktifkan fitur **Harga Jual < HPP**.
## Menu Pembelian Obat
- **Fixed :**
	1. Fix nilai **Margin Harga Jual (1, 2, 3)**, **Harga Jual (1, 2, 3)** dan **Diskon Harga Jual (1, 2, 3)** yang tidak sesuai dengan input pada saat disimpan.

# [1.8.0.6] - 2018-08-08
## Menu Data Jurnal Keuangan
- **Changed :**
	1. Ketika menambah jurnal keuangan, input Akun Kredit dipindah sebelum Akun Debit. Supaya lebih mudah menentukan "dari akun mana - ke akun mana".

## Menu Laporan Buku Besar
- **Removed :**
	1. Hapus informasi total mutasi pada hasil cetak laporan buku besar.